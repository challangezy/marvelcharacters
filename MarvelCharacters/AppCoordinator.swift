//
//  AppCoordinator.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 19.08.2021.
//

import UIKit

class AppCoordinator {
    
    private let window: UIWindow
    var navigationController: UINavigationController!
    init(window: UIWindow){
        self.window = window
    }
    
    func start(){
        let viewController = MainViewController.instantiate()
        viewController.coordinator = self
        self.navigationController = UINavigationController(rootViewController: viewController)
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    func characterDetail(character: Character){
        let viewController = CharacterDetailViewController.instantiate()
        viewController.coordinator = self
        viewController.character = character
        self.navigationController.showDetailViewController(viewController, sender: self)
    }
}
