//
//  CharacterDetailViewController.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 16.08.2021.
//

import UIKit
import RxSwift
import RxCocoa

class CharacterDetailViewController: UIViewController, Storyboarded {
    weak var coordinator: AppCoordinator?
    
    var character: Character!
    var characterDetailViewModel: CharacterDetailViewModel!
    
    @IBOutlet weak var imageViewCharacter: UIImageView!
    @IBOutlet weak var labelCharacterTitle: UILabel!
    @IBOutlet weak var labelCharacterDescription: UILabel!
    @IBOutlet weak var tableViewCharacterComics: UITableView!
    
    
    let disposeBag = DisposeBag()
    private var comicListViewModel = ComicsListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewCharacterComics.contentInsetAdjustmentBehavior = .never
        tableViewCharacterComics.tableFooterView = UIView()
        
        characterDetailViewModel = CharacterDetailViewModel(character: character)
        setupUI()
        
        comicListViewModel.fetchComicViewModels(characterId: character.id).observe(on: MainScheduler.instance).bind(to:
          tableViewCharacterComics.rx.items(cellIdentifier: "comicCell")) {index, viewModel, cell in
            cell.textLabel?.text = viewModel.displayText
            
          }.disposed(by: disposeBag)
        
    }
    
    func setupUI() {
        imageViewCharacter.download(from: characterDetailViewModel.imageUrl)
        labelCharacterTitle.text = characterDetailViewModel.title
        labelCharacterDescription.text = characterDetailViewModel.description
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
