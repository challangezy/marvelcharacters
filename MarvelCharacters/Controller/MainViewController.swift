//
//  MainViewController.swift.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 15.08.2021.
//

import UIKit
import RxSwift
import RxCocoa

class MainViewController: UIViewController, Storyboarded {
    weak var coordinator: AppCoordinator?
    
    let disposeBag = DisposeBag()
    private var viewModel = CharactersListViewModel()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = viewModel.title
        navigationController?.navigationBar.prefersLargeTitles = true
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.tableFooterView = UIView()
        
        viewModel.fetchCharacterViewModels().observe(on: MainScheduler.instance).bind(to:
          tableView.rx.items(cellIdentifier: "cell")) {index, viewModel, cell in
            cell.textLabel?.text = viewModel.displayText
            cell.imageView?.download(from: viewModel.imageUrl)
            
          }.disposed(by: disposeBag)
        
          tableView.rx
            .modelSelected(CharacterViewModel.self)
            .subscribe(onNext: { [self] characterViewModel in
                coordinator?.characterDetail(character: characterViewModel.character)
            })
          .disposed(by: disposeBag)
    }
    
}

