//
//  Character.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 15.08.2021.
//

import Foundation

class Character: Codable{
    let id: Int
    let name, resultDescription: String
    let modified: String
    let thumbnail: Thumbnail
    let resourceURI: String

    enum CodingKeys: String, CodingKey {
        case id, name
        case resultDescription = "description"
        case modified, thumbnail, resourceURI
    }

    init(id: Int, name: String, resultDescription: String, modified: String, thumbnail: Thumbnail, resourceURI: String) {
        self.id = id
        self.name = name
        self.resultDescription = resultDescription
        self.modified = modified
        self.thumbnail = thumbnail
        self.resourceURI = resourceURI
    }
}
