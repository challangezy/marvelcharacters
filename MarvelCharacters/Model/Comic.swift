//
//  Comic.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 18.08.2021.
//

import Foundation

class Comic: Codable {
    let id: Int
    let title: String
    let issueNumber: Int?
    let variantDescription, resultDescription: String?
    let modified: String
    let isbn, upc, diamondCode, ean: String?
    let issn, format: String?
    let pageCount: Int?
    let resourceURI: String?
    let thumbnail: Thumbnail?
    let images: [Thumbnail]?

    enum CodingKeys: String, CodingKey {
        case id
        case title, issueNumber, variantDescription
        case resultDescription = "description"
        case modified, isbn, upc, diamondCode, ean, issn, format, pageCount, resourceURI, thumbnail, images
    }

    init(id: Int, title: String, issueNumber: Int?, variantDescription: String?, resultDescription: String?, modified: String, isbn: String?, upc: String?, diamondCode: String?, ean: String?, issn: String?, format: String?, pageCount: Int?, resourceURI: String?, thumbnail: Thumbnail?, images: [Thumbnail]?) {
        self.id = id
        self.title = title
        self.issueNumber = issueNumber
        self.variantDescription = variantDescription
        self.resultDescription = resultDescription
        self.modified = modified
        self.isbn = isbn
        self.upc = upc
        self.diamondCode = diamondCode
        self.ean = ean
        self.issn = issn
        self.format = format
        self.pageCount = pageCount
        self.resourceURI = resourceURI
        self.thumbnail = thumbnail
        self.images = images
    }
}
