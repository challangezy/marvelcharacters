//
//  Thumbnail.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 19.08.2021.
//

import Foundation

class Thumbnail: Codable {
    let path: String
    let thumbnailExtension: Extension

    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }

    init(path: String, thumbnailExtension: Extension) {
        self.path = path
        self.thumbnailExtension = thumbnailExtension
    }
}

enum Extension: String, Codable {
    case gif = "gif"
    case jpg = "jpg"
}
