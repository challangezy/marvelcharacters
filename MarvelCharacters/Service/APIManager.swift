//
//  APIManager.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 15.08.2021.
//

import Foundation

class APIManager: NSObject {
    
    static let shared = APIManager()

    fileprivate func getUrl(_ endPoint: String) -> URL{
        let timeStamp = Int64(Date().timeIntervalSince1970 * 1000)
        let apiKey = "77cc935d009aba55c3d934409ac32e2c"
        let privateKey = "c27ab4ed617102bdd70af94d419e47ad783e6198"
        let hash = "\(timeStamp)\(privateKey)\(apiKey)".md5()
        let urlString = "http://gateway.marvel.com/v1/public/\(endPoint)?ts=\(timeStamp)&apikey=\(apiKey)&hash=\(hash)"
        
        guard let url = URL(string: urlString) else {fatalError()}
        
        return url
    }
    
    func fetchData(endPoint: String, method: String, completion: @escaping(Result<Data, APIError>) -> Void) {

        var urlRequest = URLRequest(url: getUrl(endPoint))
        urlRequest.httpMethod = method
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
       URLSession.shared.dataTask(with: urlRequest) {data, response, _ in
            DispatchQueue.main.async {
                guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let _ = data else {
                    completion(.failure(.responseError))
                    return
                }
                completion(.success(data!))
            }
        }.resume()
        
    }
}

enum HTTPMethod : String {
    case GET
    case POST
}

enum APIError: Error {
    case responseError
    case unknownError
}
