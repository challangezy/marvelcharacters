//
//  CharactersListService.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 15.08.2021.
//

import Foundation
import RxSwift


protocol CharactersListServiceProtocol {
    func fetchCharacters() -> Observable<[Character]>
}

class CharactersListService: CharactersListServiceProtocol {
    
    func fetchCharacters() -> Observable<[Character]> {
        return Observable.create { observer -> Disposable in
            APIManager.shared.fetchData(endPoint: "characters", method: HTTPMethod.GET.rawValue, completion:{ result in
                switch result{
                case .success(let data):
                    do{
                        let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                        let dictData = dict?["data"] as? [String: Any]
                        let results = dictData?["results"]
                        do {
                            let json = try JSONSerialization.data(withJSONObject: results as Any)
                            let decoder = JSONDecoder()
                            decoder.keyDecodingStrategy = .convertFromSnakeCase
                            let characters = try decoder.decode([Character].self, from: json)
                            observer.onNext(characters)
                        } catch {
                            observer.onError(error)
                        }
                    }
                    catch {
                        observer.onError(error)
                    }
                case .failure(let error):
                    print(error)
                    observer.onError(error)
                    
                }
            })
            
            return Disposables.create {
//                task.cancel()
            }
        }
        

    }
}
