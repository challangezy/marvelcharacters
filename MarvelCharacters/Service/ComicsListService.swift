//
//  ComicService.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 18.08.2021.
//

import Foundation
import RxSwift


protocol ComicsListServiceProtocol {
    func fetchComics(characterId: Int) -> Observable<[Comic]>
}

class ComicsListService: ComicsListServiceProtocol {

    func fetchComics(characterId: Int) -> Observable<[Comic]> {
        return Observable.create { observer -> Disposable in
            APIManager.shared.fetchData(endPoint: "characters/\(characterId)/comics", method: HTTPMethod.GET.rawValue, completion:{ result in
                switch result{
                case .success(let data):
                    do{
                        let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                        let dictData = dict?["data"] as? [String: Any]
                        let results = dictData?["results"]
                        do {
                            let json = try JSONSerialization.data(withJSONObject: results as Any)
                            let decoder = JSONDecoder()
                            decoder.keyDecodingStrategy = .convertFromSnakeCase
                            let comics = try decoder.decode([Comic].self, from: json)
                            
                            // MARK: -  Burayı rxSwift'e biraz daha aşina olduğunda düzelt
                            let sortedComics = Array(comics.filter({
                                getDate("31/12/2005").compare($0.modified.date()) == .orderedAscending
                            }).sorted(by: { $0.modified.date().compare($1.modified.date()) == .orderedDescending }).prefix(10))
                            
                            observer.onNext(sortedComics)
                        } catch {
                            observer.onError(error)
                        }
                    }
                    catch {
                        observer.onError(error)
                    }
                case .failure(let error):
                    print(error)
                    observer.onError(error)
                    
                }
            })
            
            return Disposables.create {
//                task.cancel()
            }
        }
        

    }
}
