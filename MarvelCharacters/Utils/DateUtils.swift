//
//  DateUtils.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 19.08.2021.
//

import Foundation

func getDate(_ date: String, format:String = "dd/MM/yyyy") -> Date {
    
    let formatter = DateFormatter()
    formatter.dateFormat = format
    return formatter.date(from: date)!
}

