//
//  CharacterDetailViewModel.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 16.08.2021.
//

import Foundation

struct CharacterDetailViewModel {
    let character: Character

    var title: String {
        return character.name
    }
    
    var description: String {
        return character.resultDescription
    }
    
    var imageUrl: String {
        return "\(character.thumbnail.path).\(character.thumbnail.thumbnailExtension)"
    }
    
    init(character: Character){
        self.character = character
    }
}
