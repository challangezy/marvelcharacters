//
//  CharacterViewModel.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 15.08.2021.
//

import Foundation

struct CharacterViewModel {
    let character: Character

    var displayText: String {
        return character.name
    }
    
    var id: Int {
        return character.id
    }
    
    var imageUrl: String {
        return "\(character.thumbnail.path).\(character.thumbnail.thumbnailExtension)"
    }
    
    init(character: Character){
        self.character = character
    }
}
