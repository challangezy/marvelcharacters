//
//  CharactersListViewModel.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 15.08.2021.
//

import Foundation
import RxSwift

final class CharactersListViewModel {
    let title = "Marvel Characters"
    private let charactersService: CharactersListServiceProtocol
    
    init(charactersService: CharactersListServiceProtocol = CharactersListService()) {
        self.charactersService = charactersService
    }
    
    func fetchCharacterViewModels() -> Observable<[CharacterViewModel]> {
        charactersService.fetchCharacters().map{$0.map {
            CharacterViewModel(character: $0)
        } }
    }
}
