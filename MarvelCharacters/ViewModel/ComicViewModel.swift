//
//  ComicViewModel.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 18.08.2021.
//

import Foundation

struct ComicViewModel {
    let comic: Comic

    var displayText: String {
        return comic.title
    }
    
    var id: Int {
        return comic.id
    }
    
    init(comic: Comic){
        self.comic = comic
    }
}
