//
//  ComicsListViewModel.swift
//  MarvelCharacters
//
//  Created by Zeynep Turgut on 18.08.2021.
//

import Foundation
import RxSwift

class ComicsListViewModel{
    
    private let comicsService: ComicsListServiceProtocol
    
    init(comicsService: ComicsListServiceProtocol = ComicsListService()) {
        self.comicsService = comicsService
    }
    
    func fetchComicViewModels(characterId: Int) -> Observable<[ComicViewModel]> {
        comicsService.fetchComics(characterId: characterId).map{$0.map {
            ComicViewModel(comic: $0)
        } }
    }
}
